import http from 'k6/http';
import { check, sleep, group } from 'k6';

export let options = {
  stages: [
    { duration: '1m', target: 10 },    // Ramp up to 10 virtual users in 1 minute
    { duration: '3m', target: 10 },    // Stay at 10 virtual users for 3 minutes
    { duration: '1m', target: 0 },     // Ramp down to 0 virtual users in 1 minute
  ],
  thresholds: {
    http_req_duration: ['p(95)<500'],  // 95% of requests should complete within 500ms
    http_req_failed: ['rate<0.1'],     // Error rate should be less than 10%
  },
};

export default function () {
  group('Test Scenario', function () {
    // Step 1: Send an HTTP GET request
    let response = http.get('https://www.example.com');
    check(response, {
      'is status 200': (r) => r.status === 200,
    });

    // Step 2: Simulate some processing time
    sleep(2); // Sleep for 2 seconds

    // Step 3: Send an HTTP POST request
    response = http.post('https://www.example.com/login', { username: 'testuser', password: 'password' });
    check(response, {
      'is status 200': (r) => r.status === 200,
    });

    // Step 4: Simulate some processing time
    sleep(1); // Sleep for 1 second

    // Step 5: Send an HTTP request with custom metrics
    response = http.get('https://www.example.com/dashboard');
    check(response, {
      'is status 200': (r) => r.status === 200,
    });

    // Step 6: Simulate some processing time
    sleep(3); // Sleep for 3 seconds

    // Step 7: Send an HTTP request with custom headers
    response = http.get('https://www.example.com/profile', { headers: { 'Authorization': 'Bearer token' } });
    check(response, {
      'is status 200': (r) => r.status === 200,
    });
  });
}
